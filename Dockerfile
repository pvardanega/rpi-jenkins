FROM resin/rpi-raspbian:latest
MAINTAINER Pierre-Jean Vardanega <pierrejean.vardanega@gmail.com>

ENV DEBIAN_FRONTEND noninteractive
ENV JENKINS_VERSION 2.71
ENV JENKINS_HOME /var/jenkins_home
ENV JENKINS_SLAVE_AGENT_PORT 50000
ENV NODEJS_VERSION 7.10.1

RUN apt-get -y update \
  && apt-get install -y --no-install-recommends curl wget git maven openjdk-8-jdk python texinfo build-essential ant

RUN curl -fL -o /opt/jenkins.war https://repo.jenkins-ci.org/public/org/jenkins-ci/main/jenkins-war/$JENKINS_VERSION/jenkins-war-$JENKINS_VERSION.war
RUN adduser --disabled-login --gecos "" --shell /bin/sh jenkins
RUN mkdir $JENKINS_HOME
RUN chown -R jenkins:jenkins $JENKINS_HOME
RUN chmod 644 /opt/jenkins.war
RUN mkdir /home/jenkins/.m2/
ADD settings.xml /home/jenkins/.m2/settings.xml

# Install nodejs 7, npm et angular cli
RUN curl -fL -o /opt/node-v$NODEJS_VERSION-linux-armv7l.tar.gz https://nodejs.org/dist/v$NODEJS_VERSION/node-v$NODEJS_VERSION-linux-armv7l.tar.gz
RUN tar -xvf /opt/node-v$NODEJS_VERSION-linux-armv7l.tar.gz -C /opt
RUN cp -R /opt/node-v$NODEJS_VERSION-linux-armv7l/* /usr/local/
RUN npm install -g @angular/cli

# Install docker
RUN curl -sSL https://get.docker.com | sh
# Fix docker-client issue with libjffi for arm: https://github.com/spotify/docker-client/issues/477
RUN git clone https://github.com/jnr/jffi.git /tmp/jffi
RUN ant jar -f /tmp/jffi/build.xml
RUN cp /tmp/jffi/build/jni/libjffi-1.2.so /usr/lib
RUN chmod 644 /usr/lib/libjffi-1.2.so

VOLUME ${JENKINS_HOME}

WORKDIR ${JENKINS_HOME}

EXPOSE 8080 ${JENKINS_SLAVE_AGENT_PORT}

CMD "java" ${JAVA_OPTS} "-jar" "/opt/jenkins.war"
